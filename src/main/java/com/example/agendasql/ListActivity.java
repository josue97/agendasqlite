package com.example.agendasql;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;



import java.util.ArrayList;

import base.de.datos.AgendaContac;
import base.de.datos.Contacto;

public class ListActivity extends android.app.ListActivity {
    private MyArrayAdapter adapter;
    ArrayList<Contacto> contactos;
    ArrayList<Contacto> filter;
    private AgendaContac agendaContactos;
    private Button btnNuevo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        btnNuevo=(Button) findViewById(R.id.btnNuevo);
        agendaContactos= new AgendaContac(this);


        llenarlist();
        adapter= new MyArrayAdapter(this,R.layout.activity_contactos,contactos);
        String str = adapter.contactos.get(1).getNombre();
        setListAdapter(adapter);


        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    class MyArrayAdapter extends ArrayAdapter<Contacto>{
        private Context context;
        private int textViewResourceId;
        private ArrayList <Contacto> contactos;

        public  MyArrayAdapter(Context context, int resource, ArrayList<Contacto> contactos){
            super(context, resource);
            this.context = context;
            this.textViewResourceId = resource;
            this.contactos = contactos;
        }

        public View getView(final int position, View converView, ViewGroup viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId,null);
            TextView lblNombre = (TextView)view.findViewById(R.id.lblNombre);
            TextView lblTelefono = (TextView)view.findViewById(R.id.txttel1);

            Button btnModificar = (Button)view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button)view.findViewById(R.id.btnBorrar);

            if(contactos.get(position).getFavorito()>0){
                lblNombre.setTextColor((Color.BLUE));
                lblTelefono.setTextColor((Color.BLUE));
            }
            else{
                lblNombre.setTextColor((Color.BLACK));
                lblTelefono.setTextColor((Color.BLACK));
            }

            lblNombre.setText(contactos.get(position).getNombre());
            lblTelefono.setText(contactos.get(position).getTelefono1());

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    agendaContactos.openDataBase();
                    agendaContactos.deleteContacto(contactos.get(position).get_ID());
                    agendaContactos.cerrar();
                    contactos.remove(position);
                    notifyDataSetChanged();
                }
            });

            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto", contactos.get(position));

                    Intent i = new Intent();
                    i.putExtras(bundle);
                    setResult(Activity.RESULT_OK,i);
                    finish();
                }
            });
            return view;

        }

    }

    private void llenarlist(){
        agendaContactos.openDataBase();
        contactos=agendaContactos.allContactos();
        agendaContactos.cerrar();
    }

}
