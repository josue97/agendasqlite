package base.de.datos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class AgendaContac {

    private Context context;
    private DBHELPER dbhelper;
    private SQLiteDatabase db;

    private String [] columnToRead = new String[]{
            Tablas.Contacto._ID,
            Tablas.Contacto.NOMBRE,
            Tablas.Contacto.TELEFONO1,
            Tablas.Contacto.TELEFONO2,
            Tablas.Contacto.DOMICILIO,
            Tablas.Contacto.NOTAS,
            Tablas.Contacto.FAVORITO

    };

    public AgendaContac(Context context) {
        this.context = context;
        dbhelper = new DBHELPER(this.context);
    }

    public void openDataBase(){

        db= dbhelper.getWritableDatabase();
    }

    public long insertarContacto (Contacto c){
        ContentValues values = new ContentValues();
        values.put(Tablas.Contacto.NOMBRE, c.getNombre());
        values.put(Tablas.Contacto.TELEFONO1, c.getTelefono1());
        values.put(Tablas.Contacto.TELEFONO2, c.getTelefono2());
        values.put(Tablas.Contacto.DOMICILIO, c.getDomicilio());
        values.put(Tablas.Contacto.NOTAS, c.getNotas());
        values.put(Tablas.Contacto.FAVORITO, c.getFavorito());

        return db.insert(Tablas.Contacto.TABLE_NAME, null,values);
    }

    public long UpdateContactos (Contacto c, long id){
        ContentValues values = new ContentValues();
        values.put(Tablas.Contacto.NOMBRE, c.getNombre());
        values.put(Tablas.Contacto.TELEFONO1, c.getTelefono1());
        values.put(Tablas.Contacto.TELEFONO2, c.getTelefono2());
        values.put(Tablas.Contacto.DOMICILIO, c.getDomicilio());
        values.put(Tablas.Contacto.NOTAS, c.getNotas());
        values.put(Tablas.Contacto.FAVORITO, c.getFavorito());

        String criterio = Tablas.Contacto._ID + " = " + id;
        return db.update(Tablas.Contacto.TABLE_NAME, values,criterio, null);
    }

    public int deleteContacto (long id){
        String criterio = Tablas.Contacto._ID + " = " + id;

        return db.delete(Tablas.Contacto.TABLE_NAME, criterio, null);
    }

    public Contacto readContacto(Cursor cursor){
        Contacto c = new Contacto();
        c.set_ID(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setTelefono1(cursor.getString(2));
        c.setTelefono2(cursor.getString(3));
        c.setDomicilio(cursor.getString(4));
        c.setNotas(cursor.getString(5));
        c.setFavorito(cursor.getInt(6));
        return c;
    }

    public Contacto getContacto(long id){
        Contacto contacto = null;
        SQLiteDatabase db = dbhelper.getReadableDatabase();
        Cursor c= db.query(Tablas.Contacto.TABLE_NAME, columnToRead, Tablas.Contacto._ID + " = ? ", new String[] {String.valueOf(id)}, null,null,null);

        if(c.moveToFirst()){
            contacto = readContacto(c);
        }


        c.close();
        return contacto;
    }


    public ArrayList<Contacto> allContactos(){

        ArrayList<Contacto> contactos = new ArrayList<Contacto>();

        Cursor cursor = db.query(Tablas.Contacto.TABLE_NAME, null,null,null,null,null,null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Contacto c = readContacto(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;
    }

    public void cerrar(){
        dbhelper.close();
    }

}
